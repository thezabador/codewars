package net.zabador.codewars.sum_of_odd_numbers

/** Created by Skye Schneider. The mad king of Domino's in the year of our Lord 2020-01-16.
 * https://www.codewars.com/kata/55fd2d567d94ac3bc9000064/kotlin
 *
 *
 *
 Given the triangle of consecutive odd numbers:

                1
            3       5
        7       9       11
    13      15      17      19
21      23      25      27      29
...
Calculate the row sums of this triangle from the row index (starting at index 1) e.g.:

rowSumOddNumbers(1); // 1
rowSumOddNumbers(2); // 3 + 5 = 8

 * */
fun rowSumOddNumbers(n: Int) = n * n * n