package net.zabador.codewars.even_numbers_in_array

import junit.framework.TestCase.assertEquals
import org.junit.Test

/** Created by Skye Schneider. The mad king of Domino's in the year of our Lord 2020-01-16.
 *  https://www.codewars.com/kata/5a431c0de1ce0ec33a00000c/train/kotlin
 * */
class EvenNumbersInArrayTest {
    @Test
    fun basicTests() {
        assertEquals(listOf(4, 6, 8), evenNumbers(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9), 3))
        assertEquals(listOf(-8, 26), evenNumbers(listOf(-22, 5, 3, 11, 26, -6, -7, -8, -9, -8, 26), 2))
        assertEquals(listOf(6), evenNumbers(listOf(6, -25, 3, 7, 5, 5, 7, -3, 23), 1))
    }
}