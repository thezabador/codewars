package net.zabador.codewars.even_numbers_in_array

/** Created by Skye Schneider. The mad king of Domino's in the year of our Lord 2020-01-16.
 *  https://www.codewars.com/kata/5a431c0de1ce0ec33a00000c/train/kotlin
 *
 *
Given an array of digital numbers, return a new array of length number containing the last even numbers from the original array (in the same order). The original array will be not empty and will contain at least "number" even numbers.

For example:

([1, 2, 3, 4, 5, 6, 7, 8, 9], 3) => [4, 6, 8]
([-22, 5, 3, 11, 26, -6, -7, -8, -9, -8, 26], 2) => [-8, 26]
([6, -25, 3, 7, 5, 5, 7, -3, 23], 1) => [6]
 * */
fun evenNumbers(array: List<Int>, number: Int): List<Int> {
    return array.filter { it % 2 == 0 }.takeLast(number)
}