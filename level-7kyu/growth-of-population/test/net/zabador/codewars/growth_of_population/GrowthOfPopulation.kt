package net.zabador.codewars.growth_of_population

import junit.framework.TestCase.assertEquals
import org.junit.Test

/** Created by Skye Schneider. The mad king of Domino's in the year of our Lord 2020-01-16.
 *  https://www.codewars.com/kata/563b662a59afc2b5120000c6/train/kotlin
 * */
class GrowthOfPopulation {

    private fun testing(actual:Int, expected:Int) {
        assertEquals(expected.toLong(), actual.toLong())
    }
    @Test
    fun test1() {
        println("Fixed Tests: nbYear")
        testing(nbYear(1500, 5.0, 100, 5000), 15)
        testing(nbYear(1500000, 2.5, 10000, 2000000), 10)

    }
}