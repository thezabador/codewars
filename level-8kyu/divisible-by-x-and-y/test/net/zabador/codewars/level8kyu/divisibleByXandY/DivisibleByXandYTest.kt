package net.zabador.codewars.level8kyu.divisibleByXandY

import junit.framework.TestCase.assertEquals
import org.junit.Test

/** Created by Skye Schneider. The mad king of Domino's in the year of our Lord 2020-01-16.
 *  https://www.codewars.com/kata/5545f109004975ea66000086/train/kotlin
 * */
class DivisibleByXAndYTest {

    @Test
    fun basicTests() {
        assertEquals(false, isDivisible(3, 3, 5))
        assertEquals(true, isDivisible(12, 3, 4))
        assertEquals(false, isDivisible(8, 3, 4))
        assertEquals(true, isDivisible(48, 3, 4))
        assertEquals(true, isDivisible(100, 5, 10))
        assertEquals(false, isDivisible(100, 5, 3))
        assertEquals(true, isDivisible(4, 4, 2))
        assertEquals(false, isDivisible(5, 2, 3))
        assertEquals(true, isDivisible(17, 17, 17))
        assertEquals(true, isDivisible(17, 1, 17))
    }
}
