package net.zabador.codewars.century_from_year

import junit.framework.TestCase.assertEquals
import org.junit.Test

/** Created by Skye Schneider. The mad king of Domino's in the year of our Lord 2020-01-16.
 *  https://www.codewars.com/kata/5a3fe3dde1ce0e8ed6000097/train/kotlin
 * */
class CenturyFromYearTest {

    @Test
    fun testFixed() {
        assertEquals(18, century(1705))
        assertEquals(19, century(1900))
        assertEquals(17, century(1601))
        assertEquals(20, century(2000))
        assertEquals(1,  century(89))
    }
}